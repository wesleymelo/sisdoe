package br.puc.sisdoe.service;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.puc.sisdoe.messaging.Producer;
import br.puc.sisdoe.model.ResultadoExame;
import br.puc.sisdoe.repository.ResultadoExameRepository;

@Service
public class ResultadoExameService{

	static final Logger LOG = LoggerFactory.getLogger(ResultadoExameService.class);
	
	@Autowired
	Producer messageSender;
	
	@Autowired
	private ResultadoExameRepository resultadoExameRepository;
	
	public List<ResultadoExame> findAll() {
		return resultadoExameRepository.findAll();
	}

	public List<ResultadoExame> enviaResultadoExamesAoSUS() {
		List<ResultadoExame> resultadoExames = resultadoExameRepository.findAll(); 
		messageSender.sendMessage(resultadoExames);
		return resultadoExames;
	}
	
	public List<ResultadoExame> enviaResultadoExamesAoSUS(List<ResultadoExame> resultadoExames) {
		messageSender.sendMessage(resultadoExames);
		return resultadoExames;
	}

	
}
