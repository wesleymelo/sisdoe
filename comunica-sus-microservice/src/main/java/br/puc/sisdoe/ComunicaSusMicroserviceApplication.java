package br.puc.sisdoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComunicaSusMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComunicaSusMicroserviceApplication.class, args);
	}
}
