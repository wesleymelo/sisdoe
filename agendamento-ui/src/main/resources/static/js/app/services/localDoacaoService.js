'use strict';

appSisDoeAgendamentoOnline.factory("LocalDoacaoService", [ "$resource", function($resource) {
	return $resource(
		"http://localhost:9001/localDoacao/:id", 
		{id: "@id"}, 
		{
			queryPorLatLngAndCep: { method : "GET", url: "http://localhost:9001/localDoacao/lat/:lat/lng/:lng/cep/:cep", params: {cep: "@cep", lat: "@lat", lng: "@lng"}, isArray : true},
			get:   { method : "GET" },
			queryPorCep:   { method : "GET", url: "http://localhost:9001/localDoacao/cep/:cep", params: {cep: "@cep"}, isArray: true }
		}
	);
}]);