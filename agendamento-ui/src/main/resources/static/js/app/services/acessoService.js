'use strict';

appSisDoeAgendamentoOnline.factory('AcessoService', [ 	
	"$http", function ($http) {
	    
		var AcessoService = {
				obtainUser : function() {
				      return $http.get('/user').then(
				    		  function(response){
				    			  console.log("Sucess obtainUser: ", response);
				    			  return response;
				    		  },function(response){
				    			  console.log("Error obtainUser: ", response);
				    			  return response;
				    		  }
				      );
			    },
			    
			    logout : function() {
				      return $http.post('http://localhost:9999/logout').then(
				    		  function(response){
				    			  console.log("Sucess obtainUser: ", response);
				    			  return response;
				    		  },function(response){
				    			  console.log("Error obtainUser: ", response);
				    			  return response;
				    		  }
				      );
			    }
			    
			    
		};
	    
	    return AcessoService;
}]);