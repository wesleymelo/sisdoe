'use strict';

appSisDoeAgendamentoOnline.factory("AgendamentoService", [ "$resource", function($resource) {
	return $resource(
		"http://localhost:9001/agendamento/", 
		{},
		{
			get:    { method:"GET", url: "http://localhost:9001/agendamento/:id", params: {id:"@id"} },
			getByDoador:    { method:"GET", url: "http://localhost:9001/agendamento/doador/:doador", params: {doador:"@doador"} },
			save:   { method:"POST" },
			query:  { method:"GET", isArray:true },
			update: { method:"PUT", url: "http://localhost:9001/agendamento/" },
			delete: { method:"DELETE", url: "http://localhost:9001/agendamento/:id", params: {id:"@id"} }
		}
	);
}]);
