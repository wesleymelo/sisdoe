'use strict';

appSisDoeAgendamentoOnline.factory("doadorService", [ "$resource", function($resource) {
	return $resource("http://localhost:9001/doador", {}, {
		query : {
			method : "GET",
			isArray : true
		},
		create : {
			method : "POST"
		},
		get : {
			method : "GET",
			url : "http://localhost:9001/doador/:id"
		},
		remove : {
			method : "DELETE",
			url : "http://localhost:9001/doador/:id"
		},
		update : {
			method : "PUT"
		}
	});
}]);