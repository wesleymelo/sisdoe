'use strict';

appSisDoeComunicaSUS.factory("ComunicaSusService", [ "$resource", function($resource) {
	return $resource(
		"http://localhost:9002/resultadoExame", 
		{},
		{
			enviar: { method:"POST", url: "http://localhost:9002/resultadoExame/enviaResultadosExamesSUS", isArray: true },
			query:  { method:"GET", isArray:true },
		}
	);
}]);
