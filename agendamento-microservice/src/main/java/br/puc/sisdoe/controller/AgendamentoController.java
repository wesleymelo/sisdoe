package br.puc.sisdoe.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.puc.sisdoe.model.Agendamento;
import br.puc.sisdoe.model.Doador;
import br.puc.sisdoe.model.LocalDoacao;
import br.puc.sisdoe.model.enumerate.Sexo;
import br.puc.sisdoe.service.AgendamentoService;
import br.puc.sisdoe.service.DoadorService;
import br.puc.sisdoe.service.LocalDoacaoService;

@CrossOrigin(maxAge = 3600) 
@RestController
@RequestMapping("/agendamento")
public class AgendamentoController {
	
	@Autowired
	private AgendamentoService agendamentoService;
	
	@Autowired
	private DoadorService doadorService;
	
	@Autowired
	private LocalDoacaoService localDoacaoService;

	//@PostConstruct
	@SuppressWarnings("unused")
	private void init() {
		
		String data = "10/10/2015";
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
        try {
			date = df.parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Doador doador = new Doador();
		doador.setNome("Wesley Melo");
		doador.setCpf("00000000000");
		doador.setEmail("wesley@mail.com");
		doador.setDataNascimento(date);
		doador.setCidade("Brasília");
		doador.setUf("DF");
		doador.setJadoou(true);
		doador.setRg(1111111);
		doador.setRgUf("DF");
		doador.setSexo(Sexo.MASCULINO.getSigla());
		doador.setTelefone("06135321526");
		doador.setCelular("061984456902");
		
		LocalDoacao localDoacao = new LocalDoacao();
		localDoacao.setNome("Laboratório Sabin Santa Maria Shopping");
		localDoacao.setUf("DF");
		localDoacao.setCidade("Brasília");
		localDoacao.setBairro("Santa Maria");
		localDoacao.setEndereco("EQ 304/307 - Santa Maria, Brasília - DF, 72504-305");
		localDoacao.setMunicipio("Brasíli");
		localDoacao.setCep("72504305");
		localDoacao.setLatitude(-16.03062650);
		localDoacao.setLongitude(-48.02834700);
		
		localDoacao = localDoacaoService.save(localDoacao);
		doador = doadorService.save(doador);
		
		Agendamento agendamento = new Agendamento();
		agendamento.setIdDoador(doador.getId());
		agendamento.setIdLocalDoacao(localDoacao.getId());
		agendamento.setDataHora(date);
		agendamentoService.save(agendamento);
	}
	
	@PreAuthorize("hasAuthority('AGENDAMENTO_READ')")
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Agendamento>> list() {
		List<Agendamento> agendamentos = agendamentoService.findAll();
		return new ResponseEntity<List<Agendamento>>(agendamentos, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('AGENDAMENTO_READ')")
	@RequestMapping(value = "/{id}",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Agendamento> findById(@PathVariable("id") Long id) {
		
		Agendamento agendamento = agendamentoService.findById(id);
		
		if(agendamento == null){
			return new ResponseEntity<Agendamento>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Agendamento>(agendamento, HttpStatus.OK);
	}
	
	@PreAuthorize("hasAuthority('AGENDAMENTO_READ')")
	@RequestMapping(value = "/doador/{doador}",  method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Agendamento> findByIdDoador(@PathVariable("doador") Long idDoador) {
		
		Agendamento agendamento = agendamentoService.buscaAgendamentoPorDoador(idDoador);
		
		if(agendamento == null){
			return new ResponseEntity<Agendamento>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Agendamento>(agendamento, HttpStatus.OK);
	}

	@PreAuthorize("hasAuthority('AGENDAMENTO_WRITE')")
	@RequestMapping(method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Agendamento> add(@RequestBody Agendamento agendamento) {
		Agendamento agendamentoSalvo = agendamentoService.save(agendamento);
		return new ResponseEntity<Agendamento>(agendamentoSalvo, HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('AGENDAMENTO_WRITE')")
	@RequestMapping(method = RequestMethod.PUT, 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Agendamento> update(@RequestBody Agendamento agendamento) {
		Agendamento agendamentoEncontrado = agendamentoService.findById(agendamento.getId());
		
		if(agendamentoEncontrado == null){
			return new ResponseEntity<Agendamento>(HttpStatus.NOT_FOUND);
		}

		Agendamento agendamentoSalvo = agendamentoService.save(agendamento);
		return new ResponseEntity<Agendamento>(agendamentoSalvo, HttpStatus.CREATED);
	}

	@PreAuthorize("hasAuthority('AGENDAMENTO_WRITE')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Agendamento> delete(@PathVariable("id") Long id) {
		Agendamento agendamentoEncontrado = agendamentoService.findById(id);
		
		if(agendamentoEncontrado == null){
			return new ResponseEntity<Agendamento>(HttpStatus.NOT_FOUND);
		}
		
		agendamentoService.delete(agendamentoEncontrado);
		return new ResponseEntity<Agendamento>(new Agendamento(), HttpStatus.OK);
	}

}
